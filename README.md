# infiniteSlider

Implement an "infinite" image slider:
  1. Switching pictures with a swipe
  2. After showing the last picture - the show continues further from the first in a circle
  3. Each picture has a title, which should also change along with the picture
  4. Pictures switch from right to left and left to right
  5. The text remains in place, it is simply replaced after the end of the image switching animation
  6. Path to pictures and titles here - https://lovetest.me/a_test_1/test_app.json
     They need to be downloaded / cache checked/updated every time the application starts.